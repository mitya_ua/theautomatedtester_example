# Simple auto tests project for http://book.theautomatedtester.co.uk/

### Requirements
* Java 8 and up
* Apache Maven
* Google Chrome 60 and up

## Usage

```java
mvn clean test
```
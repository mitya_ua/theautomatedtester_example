package pages;

import blocks.MainMenu;
import org.openqa.selenium.WebDriver;
import templates.BaseTemplate;

public class MainPage extends BaseTemplate {

    private MainMenu mainMenu;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public MainMenu getMainMenu() {
        return mainMenu;
    }
}

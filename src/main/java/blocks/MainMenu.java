package blocks;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import pages.FirstChapterPage;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.Link;
import templates.BaseTemplate;

@Name("MainPageMenu")
@FindBy(tagName = "ul")
public class MainMenu extends HtmlElement {

    public enum MainMenuLink {
        FIRST_CHAPTER, SECOND_CHAPTER
    }

    @FindBy(linkText = "Chapter1")
    private Link fitstChapter;

    @FindBy(linkText = "Chapter2")
    private Link secondChapter;

    public BaseTemplate clickOnMenuItem(MainMenuLink link, WebDriver driver) {
        switch (link) {
            case FIRST_CHAPTER:
                fitstChapter.click();
                return new FirstChapterPage(driver);
            default:
                throw new IllegalArgumentException("Isn't supported yet");
        }
    }
}

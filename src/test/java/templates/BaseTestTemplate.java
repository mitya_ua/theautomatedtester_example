package templates;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class BaseTestTemplate {
    protected WebDriver driver;

    @BeforeClass
    protected void initDriver() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @AfterClass
    protected void stopDriver() {
        if (driver != null) {
            driver.quit();
        }
    }
}

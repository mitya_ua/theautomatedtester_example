package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.Link;
import ru.yandex.qatools.htmlelements.element.TextBlock;
import templates.BaseTemplate;

public class FirstChapterPage extends BaseTemplate {

    @FindBy(id = "divontheleft")
    private TextBlock assertText;

    @FindBy(linkText = "Home Page")
    private Link mainPageLink;

    public FirstChapterPage(WebDriver driver) {
        super(driver);
    }

    public boolean isAssertTextExist() {
        return assertText.exists();
    }

    public MainPage returnToMainPage() {
        mainPageLink.click();
        return new MainPage(driver);
    }
}

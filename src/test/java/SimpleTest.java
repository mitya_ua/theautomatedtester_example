import blocks.MainMenu;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.FirstChapterPage;
import pages.MainPage;
import templates.BaseTestTemplate;

public class SimpleTest extends BaseTestTemplate {

    private final static String URL = "http://book.theautomatedtester.co.uk/";
    private MainPage mainPage;
    private FirstChapterPage firstChapterPage;

    @BeforeClass
    public void openMainPage() {
        driver.get(URL);
    }

    @Test
    public void textShoulBeExistOnFirstChapterPageTest() {
        mainPage = new MainPage(driver);
        firstChapterPage =
                (FirstChapterPage) mainPage.getMainMenu()
                        .clickOnMenuItem(MainMenu.MainMenuLink.FIRST_CHAPTER, driver);
        Assert.assertTrue(firstChapterPage.isAssertTextExist());
    }

    @Test(dependsOnMethods = "textShoulBeExistOnFirstChapterPageTest")
    private void mainPageShouldBeOpenedAfterReturnFromChapterPageTest() {
        mainPage = firstChapterPage.returnToMainPage();
        Assert.assertTrue(driver.getCurrentUrl().equals(URL));
    }
}
